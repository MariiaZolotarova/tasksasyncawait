﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Context;

namespace ProjectStructure.UnitTests.Helpers
{
    public static class DbContextHelper
    {
        public static ProjectContext GetProjectContext(string database)
        {
            var builder = new DbContextOptionsBuilder<ProjectContext>().UseInMemoryDatabase(database);
            var context = new ProjectContext(builder.Options);

            return context;
        }
    }
}
