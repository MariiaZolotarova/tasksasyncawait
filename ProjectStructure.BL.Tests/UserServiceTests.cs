using System;
using System.Linq;
using AutoMapper;
using FakeItEasy;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.BL.UnitOfWork;
using ProjectStructure.Mapping;
using ProjectStructure.Models;
using ProjectStructure.Services.Implementation;
using ProjectStructure.UnitTests.Helpers;
using Xunit;

namespace ProjectStructure.UnitTests
{
    public class UserServiceTests
    {
        private readonly IMapper _mapper;

        public UserServiceTests()
        {
            var mappingProfile = new MappingProfile();
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(mappingProfile);
            });

            _mapper = new Mapper(mapperConfiguration);
        }

        [Fact]
        public async System.Threading.Tasks.Task CreateUser_WhenCreateUser_ThenUserPlusOneAsync()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("1");
            var unitOfWork = new UnitOfWork(context);
            var usersService = new UsersService(unitOfWork, _mapper);
            var birthDay = DateTime.UtcNow;
            var userDto = new UserDto
            {
                FirstName = "Abc",
                LastName = "Abc",
                Birthday = birthDay,
                Email = "asdsa",
                TeamId = null
            };

            // Act
            await usersService.CreateUserAsync(userDto);

            // Assert
            Assert.Equal(1, context.Users.Count());
            Assert.Equal(userDto.FirstName, context.Users.First().FirstName);
            Assert.Equal(userDto.TeamId, context.Users.First().TeamId);
            Assert.Equal(birthDay, context.Users.First().Birthday);
        }

        [Fact]
        public async System.Threading.Tasks.Task CreateUser_WhenCreateUserWithNullName_ThenThrowException()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("2");
            var unitOfWork = new UnitOfWork(context);
            var usersService = new UsersService(unitOfWork, _mapper);
            var birthDay = DateTime.UtcNow;
            var userDto = new UserDto
            {
                FirstName = null,
                LastName = "Abc",
                Birthday = birthDay,
                Email = "asdsa",
                TeamId = null
            };

            // Act & Assert
            await Assert.ThrowsAsync<ArgumentNullException>(() => usersService.CreateUserAsync(userDto));
        }

        [Fact]
        public async System.Threading.Tasks.Task DeleteUser_WhenDeleteUser_ThenUserMinusOneAsync()
        {
            // Arrange
            var context = DbContextHelper.GetProjectContext("3");
            var unitOfWork = new UnitOfWork(context);
            var usersService = new UsersService(unitOfWork, _mapper);
            var birthDay = DateTime.UtcNow;
            var user = new User
            {
                FirstName = "Blopy",
                LastName = "Abc",
                Birthday = birthDay,
                Email = "asdsa",
                TeamId = null,
                Id = 1,
                Phone = "666-777",
                RegisteredAt = DateTime.UtcNow
            };
            context.Users.Add(user);
            context.SaveChanges();

            // Act
            int userId = 1;
            await usersService.DeleteUserAsync(userId);

            // Assert
            Assert.Equal(0, context.Users.Count());
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUsers_GetUserByIdAsync()
        {
            // Arrange
            var unitOfWork = A.Fake<IUnitOfWork>();
            var usersService = new UsersService(unitOfWork, _mapper);
            var birthDay = DateTime.UtcNow;
            var userDto = new UserDto
            {
                FirstName = "Abc",
                LastName = "Abc",
                Birthday = birthDay,
                Email = "asdsa",
                TeamId = null
            };

            // Act
            await usersService.CreateUserAsync(userDto);

            // Assert
            A.CallTo(() => unitOfWork.Set<User>()).MustHaveHappened();
            A.CallTo(() => unitOfWork.SaveChangesAsync()).MustHaveHappened();
        }
    }
}
