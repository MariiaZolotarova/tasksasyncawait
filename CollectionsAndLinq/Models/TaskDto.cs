﻿namespace CollectionsAndLinq.Models
{
    public class TaskDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public StateEnum State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}
