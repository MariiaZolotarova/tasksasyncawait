﻿using System;

namespace CollectionsAndLinq.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? FinishedAt { get; set; }
        public StateEnum State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public UsersModel Performer { get; set; }
    }

    public enum StateEnum
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
