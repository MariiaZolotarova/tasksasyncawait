﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CollectionsAndLinq.Models;
using Newtonsoft.Json;
using Timer = System.Timers.Timer;

namespace CollectionsAndLinq
{
    class Program
    {
        private static string baseUrl = "https://localhost:44379/";
        static async Task Main()
        {
            do
            {
                Console.Clear();
                ShowMenu();
                Console.WriteLine("Enter number: ");

                string number = Console.ReadLine();

                try
                {
                    switch (number)
                    {
                        case "1":
                            Console.WriteLine("Show count of tasks of a specific user");
                            await ShowCountProjectsWithTasksAsync();
                            break;
                        case "2":
                            Console.WriteLine("Show tasks designed for a specific user");
                            await ShowDesignedTasksForSpecialUserAsync();
                            break;
                        case "3":
                            Console.WriteLine("Show tasks performed in the current (2020) year for a specific user");
                            await ShowPerformedTasksInCurrentYearAsync();
                            break;
                        case "4":
                            Console.WriteLine("Show list of teams whose members are older than 10 years");
                            await ShowListOfTeamsOlderThanAsync();
                            break;
                        case "5":
                            Console.WriteLine("Show list of users with sorted tasks");
                            await ShowListOfUsersWithSortedTasksAsync();
                            break;
                        case "6":
                            Console.WriteLine("Show struct of user" +
                                              "\n \t Last user project " +
                                              "\n \t The total number of tasks under the last project " +
                                              "\n \t The total number of incomplete or canceled tasks for the user " +
                                              "\n \t The user's longest task");
                            await StructOfUserAsync();
                            break;
                        case "7":
                            Console.WriteLine("Show struct of project:" +
                                              "\n \t Project" +
                                              "\n \t The longest task of the project" +
                                              "\n \t The shortest task of the project" +
                                              "\n \t The total number of users in the project team");
                            await StructOfProjectsAsync();
                            break;
                        case "8":
                            await CreateUserAsync();
                            break;
                        case "9":
                            await CreateTeamAsync();
                            break;
                        case "10":
                            await CreateTaskAsync();
                            break;
                        case "11":
                            await CreateProjectAsync();
                            break;
                        case "12":
                            await DeleteUserAsync();
                            break;
                        case "13":
                            await DeleteTeamAsync();
                            break;
                        case "14":
                            await DeleteTaskAsync();
                            break;
                        case "15":
                            await DeleteProjectAsync();
                            break;
                        case "16":
                            await UpdateUserAsync();
                            break;
                        case "17":
                            await UpdateTeamAsync();
                            break;
                        case "18":
                            await UpdateTaskAsync();
                            break;
                        case "19":
                            await UpdateProjectAsync();
                            break;
                        case "20":
                            await ShowUsersAsync();
                            break;
                        case "21":
                            await ShowTeamsAsync();
                            break;
                        case "22":
                            await ShowTasksAsync();
                            break;
                        case "23":
                            await ShowProjectsAsync();
                            break;
                        case "24":
                            UpdateRandomTask();
                            break;
                    }
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                }

                Console.WriteLine("Press any key...");
                Console.ReadKey();
            } while (true);
        }

        private static async Task UpdateRandomTask()
        {
            var taskId = await MarkRandomTaskWithDelay(1000);
            Console.WriteLine(taskId);
        }

        private static Task<int> MarkRandomTaskWithDelay(int interval)
        {
            var tcs = new TaskCompletionSource<int>();
            var timer = new Timer(interval);
            timer.Elapsed += async (sender, args) =>
            {
                var tasks = await GetDataFromEndpointAsync<List<TaskModel>>("Tasks");
                var rnd = new Random();
                var index = rnd.Next(tasks.Count);
                var task = tasks[index];
                var taskDto = new TaskDto
                {
                    Name = task.Name,
                    Description = task.Description,
                    ProjectId = task.ProjectId,
                    PerformerId = task.PerformerId,
                    State = StateEnum.Finished
                };

                try
                {
                    await UpdateDataToEndpointAsync($"Tasks/{task.Id}", taskDto);
                    tcs.TrySetResult(task.Id);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    tcs.TrySetException(exception);
                }
            };
            timer.Enabled = true;
            timer.AutoReset = false;

            return tcs.Task;
        }

        private static async Task ShowUsersAsync()
        {
            var users = await GetDataFromEndpointAsync<List<UsersModel>>("Users");

            foreach (var user in users)
            {
                Console.WriteLine(JsonConvert.SerializeObject(user));
            }
        }

        private static async Task ShowTeamsAsync()
        {
            var teams = await GetDataFromEndpointAsync<List<TeamsModel>>("Teams");

            foreach (var team in teams)
            {
                Console.WriteLine(JsonConvert.SerializeObject(team));
            }
        }

        private static async Task ShowTasksAsync()
        {
            var tasks = await GetDataFromEndpointAsync<List<TaskModel>>("Tasks");

            foreach (var task in tasks)
            {
                Console.WriteLine(JsonConvert.SerializeObject(task));
            }
        }

        private static async Task ShowProjectsAsync()
        {
            var projects = await GetDataFromEndpointAsync<List<ProjectModel>>("Projects");

            foreach (var project in projects)
            {
                Console.WriteLine(JsonConvert.SerializeObject(project));
            }
        }

        private static async Task ShowCountProjectsWithTasksAsync()
        {
            var userId = ReadUserId();
            var filteredProjects = await GetDataFromEndpointAsync<Dictionary<string, int>>($"Collection/ShowCountProjectsWithTasks/{userId}");

            foreach (var projectTask in filteredProjects)
            {
                Console.WriteLine(projectTask.Key + " " + projectTask.Value);
            }
        }

        private static async Task ShowDesignedTasksForSpecialUserAsync()
        {
            var userId = ReadUserId();
            var filteredTasks = await GetDataFromEndpointAsync<List<TaskModel>>($"Collection/ShowDesignedTasksForSpecialUser/{userId}");

            foreach (var filtredTask in filteredTasks)
            {
                Console.WriteLine(filtredTask.Name);
            }
        }

        private static async Task ShowPerformedTasksInCurrentYearAsync()
        {
            var userId = ReadUserId();
            var filtredTasks = await GetDataFromEndpointAsync<List<TaskModel>>($"Collection/ShowPerformedTasksInCurrentYear/{userId}");

            foreach (var filtredTask in filtredTasks)
            {
                Console.WriteLine(filtredTask.Id + " " + filtredTask.Name);
            }
        }

        private static async Task ShowListOfTeamsOlderThanAsync()
        {
            var filtredUsers = await GetDataFromEndpointAsync<List<TeamUsers>>("Collection/ShowListOfTeamsOlderThan");

            foreach (var userTeam in filtredUsers)
            {
                Console.WriteLine(userTeam.TeamId + " " + userTeam.Name + " " + string.Join(",", userTeam.Users.Select(x => x.Email)));
            }
        }

        private static async Task ShowListOfUsersWithSortedTasksAsync()
        {
            var usersTasks = await GetDataFromEndpointAsync<List<UserTasks>>("Collection/ShowListOfUsersWithSortedTasks");

            foreach (var userTasks in usersTasks)
            {
                Console.WriteLine($"FirstName: {userTasks.User.FirstName}, Tasks: [{string.Join(", ", userTasks.Tasks.Select(x => $"(Id: {x.Id}, Length: {x.Name.Length})"))}]");
            }
        }

        private static async Task StructOfUserAsync()
        {
            var userId = ReadUserId();
            var userTask = GetDataFromEndpointAsync<UsersModel>($"Users/{userId}");
            var lastUserProjectTask = GetDataFromEndpointAsync<ProjectTasks>($"Collection/StructOfUser/{userId}");

            await Task.WhenAll(userTask, lastUserProjectTask);

            var user = userTask.Result;
            var lastUserProject = lastUserProjectTask.Result;

            if (lastUserProject == null)
            {
                Console.WriteLine("No current projects");
            }
            else
            {
                lastUserProject.User = user;
                Console.WriteLine($"User: {lastUserProject.User.Email}");
                Console.WriteLine($"LastProject: {lastUserProject.Project.Name}");
                Console.WriteLine($"TasksCount: {lastUserProject.TasksCount}");
                Console.WriteLine($"NotFinishedTasks: {lastUserProject.NotFinishedTasks}");
                Console.WriteLine($"LongestTask: {lastUserProject.LongestTask.Name}");
            }
        }

        private static async Task StructOfProjectsAsync()
        {
            var newProjects = await GetDataFromEndpointAsync<List<ProjectInfo>>("Collection/StructOfProjects");

            foreach(var project in newProjects)
            {
                Console.WriteLine($"Project: {project.Project.Name}");
                Console.WriteLine($"LongestTask: {project.LongestTask?.Name}");
                Console.WriteLine($"ShortestTask: {project.ShortestTask?.Name}");
                Console.WriteLine($"UsersCount: {project.UsersCount}");
                Console.WriteLine("-----------------------------------------------");
            }
        }

        private static async Task<T> GetDataFromEndpointAsync<T>(string endpoint)
        {
            using var client = new HttpClient();
            var response = await client.GetAsync(baseUrl + "api/" + endpoint);
            var content = await response.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<T>(content);
            return model;
        }

        private static async Task DeleteDataFromEndpointAsync(string endpoint)
        {
            using var client = new HttpClient();
            var response = await client.DeleteAsync(baseUrl + "api/" + endpoint);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Console.WriteLine("Data deleted");
            }
            else
            {
                Console.WriteLine("Error while deleting data");
            }
        }

        private static async Task SaveDataToEndpointAsync<T>(string endpoint, T body)
        {
            using var client = new HttpClient();
            var json = JsonConvert.SerializeObject(body);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(baseUrl + "api/" + endpoint, data);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Console.WriteLine("Data saved");
            }
            else
            {
                Console.WriteLine("Error while saving data");
            }
        }

        private static async Task UpdateDataToEndpointAsync<T>(string endpoint, T body)
        {
            using var client = new HttpClient();
            var json = JsonConvert.SerializeObject(body);
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync(baseUrl + "api/" + endpoint, data);

            if (response.StatusCode == HttpStatusCode.OK)
            {
                Console.WriteLine("Data updated");
            }
            else
            {
                throw new Exception($"Error while updating data. Endpont: {endpoint}, Data: {JsonConvert.SerializeObject(body)}");
            }
        }

        private static int ReadUserId()
        {
            Console.WriteLine("Enter User Id");
            var parseResult = Int32.TryParse(Console.ReadLine(), out var userId);

            if (!parseResult)
            {
                Console.WriteLine("Wrong user id");
            }

            return userId;
        }

        private static async Task CreateUserAsync()
        {
            var user = ReadUserDto();

            if (user != null)
            {
                await SaveDataToEndpointAsync("Users", user);
            }
        }

        private static UserDto ReadUserDto()
        {
            Console.WriteLine("Enter first Name");
            string firstName = Console.ReadLine();
            Console.WriteLine("Enter last Name");
            string lastName = Console.ReadLine();
            Console.WriteLine("Enter email");
            string email = Console.ReadLine();
            Console.WriteLine("Enter birthday");
            var birthdayParseResult = DateTime.TryParse(Console.ReadLine(), out var birthday);
            Console.WriteLine("Enter team Id");
            var teamIdParseResult = Int32.TryParse(Console.ReadLine(), out var teamId);

            if (string.IsNullOrWhiteSpace(firstName) || string.IsNullOrWhiteSpace(lastName)
                                                     || string.IsNullOrWhiteSpace(email) || !birthdayParseResult || !teamIdParseResult)
            {
                Console.WriteLine("Enter the correct data");
                return null;
            }

            return new UserDto
            {
                TeamId = teamId,
                Birthday = birthday,
                Email = email,
                FirstName = firstName,
                LastName = lastName
            };
        }

        private static async Task CreateTeamAsync()
        {
            var team = ReadTeamDto();

            if (team != null)
            {
                await SaveDataToEndpointAsync("Teams", team);
            }
        }

        private static TeamDto ReadTeamDto()
        {
            Console.WriteLine("Enter Name of Team");
            string name = Console.ReadLine();

            if (string.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Enter the correct data");
                return null;
            }

            return new TeamDto()
            {
                Name = name
            };
        }

        private static async Task CreateTaskAsync()
        {
            var task = ReadTaskDto();

            if (task != null)
            {
                await SaveDataToEndpointAsync("Tasks", task);
            }
        }

        private static TaskDto ReadTaskDto()
        {
            Console.WriteLine("Enter Name of Task");
            string name = Console.ReadLine();
            Console.WriteLine("Enter description");
            string description = Console.ReadLine();
            Console.WriteLine("Chose state : Created, Started, Finished, Canceled");
            var stateParseResult = Enum.TryParse(typeof(StateEnum), Console.ReadLine(), out var state);
            Console.WriteLine("Enter project Id");
            var projectIdParseResult = Int32.TryParse(Console.ReadLine(), out var projectId);
            Console.WriteLine("Enter performer Id");
            var performerIdParseResult = Int32.TryParse(Console.ReadLine(), out var performerId);

            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(description)
                                                || !stateParseResult || state == null || !projectIdParseResult || !performerIdParseResult)
            {
                Console.WriteLine("Enter the correct data");
                return null;
            }

            return new TaskDto()
            {
                Name = name,
                Description = description,
                State = (StateEnum)state,
                ProjectId = projectId,
                PerformerId = performerId
            };
        }

        private static async Task CreateProjectAsync()
        {
            var project = ReadProjectDto();

            if (project != null)
            {
                await SaveDataToEndpointAsync("Projects", project);
            }
        }

        private static ProjectDto ReadProjectDto()
        {
            Console.WriteLine("Enter Name of project");
            string name = Console.ReadLine();
            Console.WriteLine("Enter description");
            string description = Console.ReadLine();
            Console.WriteLine("Enter deadline");
            var deadLineParseResult = DateTime.TryParse(Console.ReadLine(), out var deadLine);
            Console.WriteLine("Enter author Id");
            var authorIdParseResult = Int32.TryParse(Console.ReadLine(), out var authorId);
            Console.WriteLine("Enter team Id");
            var teamIdParseResult = Int32.TryParse(Console.ReadLine(), out var teamId);

            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(description)
                                                || !deadLineParseResult || !authorIdParseResult || !teamIdParseResult)
            {
                Console.WriteLine("Enter the correct data");
                return null;
            }

            return new ProjectDto()
            {
                Name = name,
                Description = description,
                DeadLine = deadLine,
                AuthorId = authorId,
                TeamId = teamId
            };
        }
        private static async Task DeleteUserAsync()
        {
            Console.WriteLine("Enter user Id");
            var userIdParseResult = Int32.TryParse(Console.ReadLine(), out var userId);
            if (!userIdParseResult)
            {
                Console.WriteLine("Enter the correct id");
            }
            await DeleteDataFromEndpointAsync($"Users/{userId}");
        }

        private static async Task DeleteTeamAsync()
        {
            Console.WriteLine("Enter team Id");
            var teamIdParseResult = Int32.TryParse(Console.ReadLine(), out var teamId);
            if (!teamIdParseResult)
            {
                Console.WriteLine("Enter the correct id");
            }
            await DeleteDataFromEndpointAsync($"Teams/{teamId}");
        }

        private static async Task DeleteTaskAsync()
        {
            Console.WriteLine("Enter task Id");
            var taskIdParseResult = Int32.TryParse(Console.ReadLine(), out var taskId);
            if (!taskIdParseResult)
            {
                Console.WriteLine("Enter the correct id");
            }
            await DeleteDataFromEndpointAsync($"Teams/{taskId}");
        }

        private static async Task DeleteProjectAsync()
        {
            Console.WriteLine("Enter project Id");
            var projectIdParseResult = Int32.TryParse(Console.ReadLine(), out var projectId);
            if (!projectIdParseResult)
            {
                Console.WriteLine("Enter the correct id");
            }
            await DeleteDataFromEndpointAsync($"Teams/{projectId}");
        }

        private static async Task UpdateUserAsync()
        {
            Console.WriteLine("Enter user Id");
            var userIdParseResult = Int32.TryParse(Console.ReadLine(), out var userId);
            if (!userIdParseResult)
            {
                Console.WriteLine("Enter the correct id");
            }

            var user = ReadUserDto();
            if (user != null && userIdParseResult)
            {
                await UpdateDataToEndpointAsync($"Users/{userId}", user);
            }
        }

        private static async Task UpdateTeamAsync()
        {
            Console.WriteLine("Enter team Id");
            var teamIdParseResult = Int32.TryParse(Console.ReadLine(), out var teamId);
            if (!teamIdParseResult)
            {
                Console.WriteLine("Enter the correct id");
            }

            var team = ReadTeamDto();
            if (team != null && teamIdParseResult)
            {
                await UpdateDataToEndpointAsync($"Teams/{teamId}", team);
            }
        }

        private static async Task UpdateTaskAsync()
        {
            Console.WriteLine("Enter task Id");
            var taskIdParseResult = Int32.TryParse(Console.ReadLine(), out var taskId);
            if (!taskIdParseResult)
            {
                Console.WriteLine("Enter the correct id");
            }

            var task = ReadTaskDto();
            if (task != null && taskIdParseResult)
            {
                await UpdateDataToEndpointAsync($"Tasks/{taskId}", task);
            }
        }

        private static async Task UpdateProjectAsync()
        {
            Console.WriteLine("Enter project Id");
            var projectIdParseResult = Int32.TryParse(Console.ReadLine(), out var projectId);
            if (!projectIdParseResult)
            {
                Console.WriteLine("Enter the correct id");
            }

            var project = ReadProjectDto();
            if (project != null && projectIdParseResult)
            {
                await UpdateDataToEndpointAsync($"Projects/{projectId}", project);
            }
        }

        private static void ShowMenu()
        {
            Console.WriteLine("1 - Show count of tasks of a specific user");
            Console.WriteLine("2 - Show tasks designed for a specific user");
            Console.WriteLine("3 - Show tasks performed in the current (2020) year for a specific user");
            Console.WriteLine("4 - Show list of teams whose members are older than 10 years");
            Console.WriteLine("5 - Show list of users with sorted tasks");
            Console.WriteLine("6 - Show struct of user");
            Console.WriteLine("7 - Show struct of project");
            Console.WriteLine("8 - User Create");
            Console.WriteLine("9 - Team Create");
            Console.WriteLine("10 - Task Create");
            Console.WriteLine("11 - Project Create");
            Console.WriteLine("12 - User Delete");
            Console.WriteLine("13 - Team Delete");
            Console.WriteLine("14 - Task Delete");
            Console.WriteLine("15 - Project Delete");
            Console.WriteLine("16 - User Update");
            Console.WriteLine("17 - Team Update");
            Console.WriteLine("18 - Task Update");
            Console.WriteLine("19 - Project Update");
            Console.WriteLine("20 - Users Show");
            Console.WriteLine("21 - Teams Show");
            Console.WriteLine("22 - Tasks Show");
            Console.WriteLine("23 - Projects Show");
            Console.WriteLine("24 - Change Status to Finished");
        }
    }
}
