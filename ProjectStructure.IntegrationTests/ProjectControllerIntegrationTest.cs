using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.IntegrationTests
{
    public class ProjectControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public ProjectControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task WhenCreateProject_CheckIfOk()
        {
            // Arrange
            var projectDto = new ProjectDto
            {
                Name = "Abc",
                TeamId = 1,
                AuthorId = 1,
                DeadLine = DateTime.UtcNow,
                Description = "Hello"
            };
            var json = JsonConvert.SerializeObject(projectDto);

            // Act
            var response = await _client.PostAsync("api/projects", new StringContent(json, Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            var project = JsonConvert.DeserializeObject<Project>(content);

            await _client.DeleteAsync($"api/projects/{project.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(projectDto.Name, project.Name);
        }

        [Fact]
        public async Task WhenCreateProject_CheckBadRequest()
        {
            // Arrange
            var projectDto = new ProjectDto
            {
                Name = null,
                TeamId = 1,
                AuthorId = 1,
                DeadLine = DateTime.UtcNow,
                Description = "Hello"
            };
            var json = JsonConvert.SerializeObject(projectDto);

            // Act
            var response = await _client.PostAsync("api/projects", new StringContent(json, Encoding.UTF8, "application/json"));

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task WhenCreateProject_CheckException()
        {
            // Arrange
            var projectDto = new ProjectDto
            {
                Name = "aASd",
                TeamId = -1,
                AuthorId = 1,
                DeadLine = DateTime.UtcNow,
                Description = "Hello"
            };
            var json = JsonConvert.SerializeObject(projectDto);

            // Act
            var response = await _client.PostAsync("api/projects", new StringContent(json, Encoding.UTF8, "application/json"));

            // Assert
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

    }
}
