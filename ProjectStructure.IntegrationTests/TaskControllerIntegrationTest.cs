﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.IntegrationTests
{
    public class TaskControllerIntegrationTest : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;

        public TaskControllerIntegrationTest(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
        }

        [Fact]
        public async Task WhenCreateTask_CheckIfOk()
        {
            // Arrange
            var taskDto = new TaskDto
            {
                Name = "DreamTask",
                State = StateEnum.Started,
                ProjectId = 1,
                PerformerId = 1,
                Description = "description"
            };
            var json = JsonConvert.SerializeObject(taskDto);

            // Act
            var response = await _client.PostAsync("api/tasks", new StringContent(json, Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            var task = JsonConvert.DeserializeObject<BL.Context.Entity.Task>(content);

            await _client.DeleteAsync($"api/tasks/{task.Id}");

            // Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            Assert.Equal(taskDto.Name, task.Name);
        }

        [Fact]
        public async Task WhenCreateTask_CheckBadRequest()
        {
            // Arrange
            var taskDto = new TaskDto
            {
                Name = null,
                State = StateEnum.Started,
                ProjectId = 1,
                PerformerId = 1,
                Description = "description"
            };
            var json = JsonConvert.SerializeObject(taskDto);

            // Act
            var response = await _client.PostAsync("api/tasks", new StringContent(json, Encoding.UTF8, "application/json"));

            // Assert
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [Fact]
        public async Task WhenCreateTask_CheckException()
        {
            // Arrange
            var taskDto = new TaskDto
            {
                Name = "DreamTask",
                State = StateEnum.Started,
                ProjectId = 1,
                PerformerId = -1,
                Description = "description"
            };
            var json = JsonConvert.SerializeObject(taskDto);

            // Act
            var response = await _client.PostAsync("api/tasks", new StringContent(json, Encoding.UTF8, "application/json"));

            // Assert
            Assert.Equal(HttpStatusCode.InternalServerError, response.StatusCode);
        }

        [Fact]
        public async Task WhenCreateNotFinishedTask_CheckData()
        {
            // Arrange
            var userDto = new UserDto
            {
                TeamId = 1,
                LastName = "Flobby",
                Birthday = DateTime.UtcNow,
                Email = "flobby@mail",
                FirstName = "Robby"
            };
            var userResponse = await _client.PostAsync("api/users", new StringContent(JsonConvert.SerializeObject(userDto), Encoding.UTF8, "application/json"));
            var userContent = await userResponse.Content.ReadAsStringAsync();
            var user = JsonConvert.DeserializeObject<User>(userContent);
            var taskDto1 = new TaskDto
            {
                Name = "DreamTask",
                State = StateEnum.Started,
                ProjectId = 1,
                PerformerId = user.Id,
                Description = "description"
            };
            var taskDto2 = new TaskDto
            {
                Name = "LittleTask",
                State = StateEnum.Finished,
                ProjectId = 1,
                PerformerId = user.Id,
                Description = "description"
            };
            var taskDto3 = new TaskDto
            {
                Name = "GaaaTask",
                State = StateEnum.Finished,
                ProjectId = 1,
                PerformerId = user.Id,
                Description = "description"
            };

            var task1 = await CreateTaskAsync(taskDto1);
            var task2 = await CreateTaskAsync(taskDto2);
            var task3 = await CreateTaskAsync(taskDto3);

            // Act
            var result = await _client.GetAsync($"api/tasks/NotFinishedTasks/{user.Id}");
            var content = await result.Content.ReadAsStringAsync();
            var notFinishedTasks = JsonConvert.DeserializeObject<List<BL.Context.Entity.Task>>(content);

            await _client.DeleteAsync($"api/tasks/{task1.Id}");
            await _client.DeleteAsync($"api/tasks/{task2.Id}");
            await _client.DeleteAsync($"api/tasks/{task3.Id}");
            await _client.DeleteAsync($"api/users/{user.Id}");

            // Assert
            Assert.Single(notFinishedTasks);
            Assert.Equal(task1.State, notFinishedTasks[0].State);
            Assert.Equal(task1.Id, notFinishedTasks[0].Id);
        }

        private async Task<BL.Context.Entity.Task> CreateTaskAsync(TaskDto taskDto)
        {
            var response = await _client.PostAsync("api/tasks", new StringContent(JsonConvert.SerializeObject(taskDto), Encoding.UTF8, "application/json"));
            var content = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<BL.Context.Entity.Task>(content);
        }
    }
}
