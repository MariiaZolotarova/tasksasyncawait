﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;
using Task = ProjectStructure.BL.Context.Entity.Task;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService tasksService;

        public TasksController(ITasksService tasksService)
        {
            this.tasksService = tasksService;
        }
        [HttpGet]
        public async Task<List<Task>> GetTasksAsync()
        {
            return await tasksService.GetTasksAsync();
        }

        [HttpGet("NotFinishedTasks/{id}")]
        public async Task<List<Task>> GetNoFinishedTasksAsync([FromRoute]int id)
        {
            return await tasksService.GetNoFinishedTasksAsync(id);
        }

        [HttpGet("{id}")]
        public async Task<Task> GetTaskIdAsync([FromRoute] int id)
        {
            return await tasksService.GetTaskIdAsync(id);
        }
        [HttpPost]
        public async Task<ActionResult<Task>> CreateTaskAsync([FromBody] TaskDto task)
        {
            try
            {
                return Ok(await tasksService.CreateTaskAsync(task));
            }
            catch (ArgumentNullException exception)
            {
                return BadRequest(exception);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception);
            }
        }

        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task DeleteTaskAsync([FromRoute] int id)
        {
            await tasksService.DeleteTaskAsync(id);
        }
        [HttpPut("{id}")]
        public async System.Threading.Tasks.Task UpdateTaskAsync([FromRoute] int id, [FromBody] TaskDto task)
        {
            await tasksService.UpdateTaskAsync(id, task);
        }
    }
}
