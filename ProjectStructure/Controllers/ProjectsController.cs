﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using ProjectStructure.Services.Abstraction;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {

        private readonly IProjectsService projectsService;

        public ProjectsController(IProjectsService projectsService)
        {
            this.projectsService = projectsService;
        }
        
        [HttpGet]
        public async System.Threading.Tasks.Task<List<Project>> GetProjectsAsync()
        {
            return await projectsService.GetProjectsAsync();
        }

        [HttpGet("{id}")]
        public async System.Threading.Tasks.Task<Project> GetProjectsIdAsync([FromRoute]int id)
        {
            return await projectsService.GetProjectIdAsync(id);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult<Project>> CreateProjectAsync([FromBody] ProjectDto project)
        {
            try
            {
                return Ok(await projectsService.CreateProjectAsync(project));
            }
            catch (ArgumentNullException exception)
            {
                return BadRequest(exception);
            }
            catch (Exception exception)
            {
                return StatusCode(500, exception);
            }
        }

        [HttpDelete("{id}")]
        public async System.Threading.Tasks.Task DeleteProjectAsync([FromRoute] int id)
        {
            await projectsService.DeleteProjectAsync(id);
        }

        [HttpPut("{id}")]
        public async System.Threading.Tasks.Task UpdateProjectAsync([FromRoute] int id, [FromBody] ProjectDto project)
        {
            await projectsService.UpdateProjectAsync(id, project);
        }
    }
}
