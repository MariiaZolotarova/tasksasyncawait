﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.BL.Context.Entity;
using ProjectStructure.Models;
using Task = System.Threading.Tasks.Task;

namespace ProjectStructure.Services.Abstraction
{
    public interface IUsersService
    {
        Task<List<User>> GetUsersAsync();
        Task<User> GetUsersId(int id);
        Task<User> CreateUserAsync(UserDto userDto);
        Task DeleteUserAsync(int id);
        Task UpdateUserAsync(int id, UserDto userDto);
    }
}
