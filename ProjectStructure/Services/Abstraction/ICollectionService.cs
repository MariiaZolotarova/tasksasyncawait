﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ProjectStructure.Models;
using Task = ProjectStructure.BL.Context.Entity.Task;

namespace ProjectStructure.Services.Abstraction
{
    public interface ICollectionService
    {
        Task<Dictionary<string, int>> ShowCountProjectsWithTasksAsync(int userId);
        Task<List<Task>> ShowDesignedTasksForSpecialUserAsync(int userId);
        Task<List<NameId>> ShowPerformedTasksInCurrentYearAsync(int userId);
        Task<List<TeamUsers>> ShowListOfTeamsOlderThanAsync();
        Task<List<UserTasks>> ShowListOfUsersWithSortedTasksAsync();
        Task<ProjectTasks> StructOfUserAsync(int userId);
        Task<List<ProjectInfo>> StructOfProjectsAsync();
    }
}
