﻿namespace ProjectStructure.Models
{
    public class StateDto
    {
        public int TeamId { get; set; }
        public string Value { get; set; }
    }
}
