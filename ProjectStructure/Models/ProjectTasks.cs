﻿using ProjectStructure.BL.Context.Entity;

namespace ProjectStructure.Models
{
    public class ProjectTasks
    {
        public Project Project { get; set; }
        public int TasksCount { get; set; }
        public int NotFinishedTasks { get; set; }
        public Task LongestTask { get; set; }
        public User User { get; set; }
    }
}
