﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.BL.Migrations
{
    [ExcludeFromCodeCoverage]
    public partial class RequiredColumns : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tasks",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "DeadLine" },
                values: new object[] { new DateTime(2020, 7, 16, 7, 15, 7, 762, DateTimeKind.Utc).AddTicks(8792), new DateTime(2020, 7, 16, 7, 15, 7, 762, DateTimeKind.Utc).AddTicks(9374) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "DeadLine" },
                values: new object[] { new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(1338), new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(1350) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(6941), new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(7369) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(9265), new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(9279) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2020, 7, 16, 7, 15, 7, 764, DateTimeKind.Utc).AddTicks(596));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2020, 7, 16, 7, 15, 7, 764, DateTimeKind.Utc).AddTicks(1146));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(2763), new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(4333) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(5166), new DateTime(2020, 7, 16, 7, 15, 7, 763, DateTimeKind.Utc).AddTicks(5213) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Teams",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Tasks",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Projects",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "DeadLine" },
                values: new object[] { new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(6224), new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(6547) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "DeadLine" },
                values: new object[] { new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(7794), new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(7801) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(1118), new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(1411) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(2610), new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(2617) });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(3414));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(3702));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(8702), new DateTime(2020, 7, 15, 17, 0, 49, 813, DateTimeKind.Utc).AddTicks(9603) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Birthday", "RegisteredAt" },
                values: new object[] { new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(102), new DateTime(2020, 7, 15, 17, 0, 49, 814, DateTimeKind.Utc).AddTicks(128) });
        }
    }
}
