﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BL.Context.Entity;

namespace ProjectStructure.BL.Context
{
    public class ProjectContext : DbContext
    {
        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options) { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Project>().Property(x => x.CreatedAt).IsRequired();
            modelBuilder.Entity<Project>().HasKey(x=>x.Id);
            modelBuilder.Entity<Project>().Property(x => x.TeamId).IsRequired();
            modelBuilder.Entity<Project>().Property(x => x.AuthorId).IsRequired();

            modelBuilder.Entity<Task>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Task>().Property(x => x.CreatedAt).IsRequired();
            modelBuilder.Entity<Task>().HasKey(x => x.Id);
            modelBuilder.Entity<Task>().Property(x => x.State).IsRequired();
            modelBuilder.Entity<Task>().Property(x => x.ProjectId).IsRequired();
            modelBuilder.Entity<Task>().Property(x => x.PerformerId).IsRequired();

            modelBuilder.Entity<Team>().Property(x => x.Name).IsRequired();
            modelBuilder.Entity<Team>().Property(x => x.CreatedAt).IsRequired();
            modelBuilder.Entity<Team>().HasKey(x => x.Id);

            modelBuilder.Entity<User>().Property(x => x.Birthday).IsRequired();
            modelBuilder.Entity<User>().Property(x => x.FirstName).IsRequired();
            modelBuilder.Entity<User>().HasKey(x => x.Id);
            modelBuilder.Entity<User>().Property(x => x.LastName).IsRequired();
            modelBuilder.Entity<User>().Property(x => x.RegisteredAt).IsRequired();

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Author)
                .WithMany(t => t.Projects)
                .HasForeignKey(p => p.AuthorId)
                .HasPrincipalKey(t => t.Id);

            modelBuilder.Entity<Project>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Projects)
                .HasForeignKey(p => p.TeamId)
                .HasPrincipalKey(t => t.Id);

            modelBuilder.Entity<Task>()
                .HasOne(p => p.Performer)
                .WithMany(t => t.Tasks)
                .HasForeignKey(p => p.PerformerId)
                .HasPrincipalKey(t => t.Id);

            modelBuilder.Entity<User>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Users)
                .HasForeignKey(p => p.TeamId)
                .HasPrincipalKey(t => t.Id);


            var projects = new List<Project>
            {
                new Project{Id = 1, CreatedAt = DateTime.UtcNow, DeadLine = DateTime.UtcNow, TeamId = 1, AuthorId = 1, Name = "Super Project", Description = "The best project ever"},
                new Project{Id = 2, CreatedAt = DateTime.UtcNow, DeadLine = DateTime.UtcNow, TeamId = 2, AuthorId = 2, Name = "Bad Project", Description = "The worst project ever"}
            };

            var users = new List<User>
            {
                new User{Id = 1, TeamId = 1, Birthday = DateTime.UtcNow, Email = "email@gmail.com", FirstName = "Jhon", LastName = "Labovski", RegisteredAt = DateTime.UtcNow, Phone="111"},
                new User{Id = 2, TeamId = 2, Birthday = DateTime.UtcNow, Email = "email1@gmail.com", FirstName = "Will", LastName = "Deapy", RegisteredAt = DateTime.UtcNow, Phone = "222"}
            };

            var tasks = new List<Task>
            {
                new Task{Id =1, Name = "Super task", Description = "The best task ever", CreatedAt = DateTime.UtcNow, FinishedAt = DateTime.UtcNow, PerformerId = 1, ProjectId = 1, State = StateEnum.Created},
                new Task{Id =2, Name = "Hard task", Description = "The hardest task ever", CreatedAt = DateTime.UtcNow, FinishedAt = DateTime.UtcNow, PerformerId = 2, ProjectId = 2, State = StateEnum.Created}
            };
            var teams = new List<Team>
            {
                new Team{Id=1, Name = "Best team", CreatedAt = DateTime.UtcNow},
                new Team{Id=2, Name = "Loser team", CreatedAt = DateTime.UtcNow}
            };

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);

            base.OnModelCreating(modelBuilder);
        }
    }
}