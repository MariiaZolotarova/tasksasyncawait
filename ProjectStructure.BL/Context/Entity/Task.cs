﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProjectStructure.BL.Context.Entity
{
    public class Task : Entity
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public StateEnum State { get; set; }
        public Project Project { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public User Performer { get; set; }
        
    }

    public enum StateEnum
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
